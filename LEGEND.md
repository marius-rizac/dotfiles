| Function Name | Description | Source File |
| --- | --- | --- |
| `acdk` | `Generate CDK `bin/main.ts` or `lib/NameStack.ts` files` | aws.plugin.sh |
| `allow_ssh_auth` | `copy ssh public key into remote server` | ssh.plugin.sh |
| `apare` | `Restart Apache2` | server.plugin.sh |
| `atlon` | `login into ECS Atlantis container` | aws.plugin.sh |
| `awsh` | `Show a list of AWS cli commands` | aws.plugin.sh |
| `awsp` | `Set AWS_PROFILE` | aws.plugin.sh |
| `awsr` | `Set AWS_REGION` | aws.plugin.sh |
| `banip` | `Ban IP with iptables` | security.plugin.sh |
| cc | `composer` | php.plugin.sh |
| ccdu | `composer dump-autoload` | php.plugin.sh |
| cci | `composer install` | php.plugin.sh |
| ccr | `composer require` | php.plugin.sh |
| ccst | `composer status` | php.plugin.sh |
| ccu | `composer update` | php.plugin.sh |
| ccw | `composer why` | php.plugin.sh |
| ccwn | `composer why-not` | php.plugin.sh |
| cg | `cargo` | rust.plugin.sh |
| cga | `cargo add` | rust.plugin.sh |
| cgb | `cargo build` | rust.plugin.sh |
| cgbr | `cargo build --release` | rust.plugin.sh |
| cgi | `cargo install` | rust.plugin.sh |
| cgn | `cargo new` | rust.plugin.sh |
| cgr | `cargo run` | rust.plugin.sh |
| cgu | `cargo update` | rust.plugin.sh |
| clean_repo | `git_clean_repo` | git.plugin.sh |
| `cln` | `Clone a github repository` | git.plugin.sh |
| `codecept` | `Run codecept` | php.plugin.sh |
| `dcap` | `Start docker httpd in current directory (deprecated)` | docker.plugin.sh |
| dci | `dcimg` | docker.plugin.sh |
| dcim | `dcimg` | docker.plugin.sh |
| `dcimg` | `List and delete docker images` | docker.plugin.sh |
| dcimr | `docker images rm -f` | docker.plugin.sh |
| dcl | `docker ps -a` | docker.plugin.sh |
| dclean | `docker system prune -af --volumes` | docker.plugin.sh |
| dclog | `docker logs` | docker.plugin.sh |
| dclw | `watch -n 1 docker ps -a` | docker.plugin.sh |
| dcn | `docker network` | docker.plugin.sh |
| dcnl | `docker network ls` | docker.plugin.sh |
| `dcon` | `Run docker exec with bash` | docker.plugin.sh |
| `dcons` | `Run docker exec with shell` | docker.plugin.sh |
| `dcpl` | `List all docker processes` | docker.plugin.sh |
| `dcr` | `Delete stopped docker containers` | docker.plugin.sh |
| dcrm | `docker compose rm` | docker.plugin.sh |
| `dcs` | `Stop all docker images` | docker.plugin.sh |
| `dcshow` | `Run Apache server into a docker container with current directory as volume` | docker.plugin.sh |
| dcst | `docker compose stop` | docker.plugin.sh |
| dcup | `docker compose up` | docker.plugin.sh |
| `dcv` | `List and remove (`-r`) docker volumes` | docker.plugin.sh |
| `debug` | `Export php debug configurations` | php.plugin.sh |
| `dirsize` | `Calculate the size of a directory` | common.plugin.sh |
| `docrun` | `Start docker container PHP 7.4` | docker.plugin.sh |
| `doctf` | `CDW=$(pwd)` | terraform.plugin.sh |
| `docup` | `Run docker with the provided image or ubuntu:24.04 as default` | docker.plugin.sh |
| `dotinstall_helm3` | `Install helm3` | install.plugin.sh |
| `dotinstall_kubectl` | `Installing kubectl on mac` | install.plugin.sh |
| `dotinstall_minikube` | `Installing minikube on mac` | install.plugin.sh |
| dtf29 | `doctf 1.2.9` | terraform.plugin.sh |
| dtf33 | `doctf 1.3.3` | terraform.plugin.sh |
| dtfu | `docker pull registry.gitlab.com/kisphp/ci-registry/terraform:latest` | terraform.plugin.sh |
| `ec2ls` | `List EC2 instances` | aws.plugin.sh |
| `edit` | `Start IntelliJ editors in current or specified directory` | editors.plugin.sh |
| `edited` | `List changed files in GIT` | git.plugin.sh |
| ekscon | `aws eks update-kubeconfig --name` | aws.plugin.sh |
| `elre` | `Restart Elasticsearch` | server.plugin.sh |
| `eltest` | `Test communication with Elasticsearch` | server.plugin.sh |
| ga | `git add` | git.plugin.sh |
| gaa | `git add --all` | git.plugin.sh |
| gap | `git apply` | git.plugin.sh |
| gapa | `git add --patch` | git.plugin.sh |
| gau | `git add --update` | git.plugin.sh |
| gb | `git branch` | git.plugin.sh |
| gba | `git branch -a` | git.plugin.sh |
| gbd | `git branch -d` | git.plugin.sh |
| gbl | `git blame -b -w` | git.plugin.sh |
| gbnm | `git branch --no-merged` | git.plugin.sh |
| gbr | `git branch --remote` | git.plugin.sh |
| gbs | `git bisect` | git.plugin.sh |
| gbsb | `git bisect bad` | git.plugin.sh |
| gbsg | `git bisect good` | git.plugin.sh |
| gbsr | `git bisect reset` | git.plugin.sh |
| gbss | `git bisect start` | git.plugin.sh |
| gc | `git commit -v` | git.plugin.sh |
| gca | `git commit -v -a` | git.plugin.sh |
| gcam | `git commit -a -m` | git.plugin.sh |
| gcb | `git checkout -b` | git.plugin.sh |
| gcd | `git checkout develop` | git.plugin.sh |
| gcf | `git config --list` | git.plugin.sh |
| gcl | `git clone --recursive` | git.plugin.sh |
| gclean | `git clean -fd` | git.plugin.sh |
| gcm | `git checkout main` | git.plugin.sh |
| gcmsg | `git commit -m` | git.plugin.sh |
| gco | `git checkout` | git.plugin.sh |
| gcount | `git shortlog -sn` | git.plugin.sh |
| gcp | `git cherry-pick` | git.plugin.sh |
| gcpa | `git cherry-pick --abort` | git.plugin.sh |
| gcpc | `git cherry-pick --continue` | git.plugin.sh |
| gcs | `git commit -S` | git.plugin.sh |
| gcsm | `git commit -s -m` | git.plugin.sh |
| gd | `git diff` | git.plugin.sh |
| gdca | `git diff --cached` | git.plugin.sh |
| gdct | `git describe --tags `git rev-list --tags --max-count=1`` | git.plugin.sh |
| gdcw | `git diff --cached --word-diff` | git.plugin.sh |
| gdt | `git diff-tree --no-commit-id --name-only -r` | git.plugin.sh |
| gdw | `git diff --word-diff` | git.plugin.sh |
| ged | `edited` | git.plugin.sh |
| `genpass` | `Generate password` | common.plugin.sh |
| gf | `git fetch` | git.plugin.sh |
| gfa | `git fetch --all --prune` | git.plugin.sh |
| gfo | `git fetch origin` | git.plugin.sh |
| gg | `git gui citool` | git.plugin.sh |
| gga | `git gui citool --amend` | git.plugin.sh |
| ggpull | `git pull origin $(git_current_branch)` | git.plugin.sh |
| ggpush | `git push origin $(git_current_branch)` | git.plugin.sh |
| ggsup | `git branch --set-upstream-to=origin/$(git_current_branch)` | git.plugin.sh |
| ghh | `git help` | git.plugin.sh |
| gignore | `git update-index --assume-unchanged` | git.plugin.sh |
| `gitci` | `Generate Gitlab Merge Request template` | git.plugin.sh |
| `gitlocal` | `Create a local git repository and create a .git directory with remote to new local repo in the current directory` | git.plugin.sh |
| gk | `\gitk --all --branches` | git.plugin.sh |
| gke | `\gitk --all $(git log -g --pretty=%h)` | git.plugin.sh |
| gl | `git pull` | git.plugin.sh |
| glg | `git log --stat` | git.plugin.sh |
| glgg | `git log --graph` | git.plugin.sh |
| glgga | `git log --graph --decorate --all` | git.plugin.sh |
| glgm | `git log --graph --max-count=10` | git.plugin.sh |
| glgp | `git log --stat -p` | git.plugin.sh |
| glo | `git log --oneline --decorate` | git.plugin.sh |
| glog | `git log --oneline --decorate --graph` | git.plugin.sh |
| gloga | `git log --oneline --decorate --graph --all` | git.plugin.sh |
| glol | `git log --graph --pretty=` | git.plugin.sh |
| glola | `git log --graph --pretty=` | git.plugin.sh |
| glum | `git pull upstream main` | git.plugin.sh |
| gm | `git merge` | git.plugin.sh |
| gma | `git merge --abort` | git.plugin.sh |
| gmom | `git merge origin/main` | git.plugin.sh |
| gmt | `git mergetool --no-prompt` | git.plugin.sh |
| gmtvim | `git mergetool --no-prompt --tool=vimdiff` | git.plugin.sh |
| gmum | `git merge upstream/main` | git.plugin.sh |
| gp | `git push` | git.plugin.sh |
| gpd | `git push --dry-run` | git.plugin.sh |
| gpoat | `git push origin --all && git push origin --tags` | git.plugin.sh |
| gpristine | `git reset --hard && git clean -dfx` | git.plugin.sh |
| gpsup | `git push --set-upstream origin $(git_current_branch)` | git.plugin.sh |
| gpu | `git push upstream` | git.plugin.sh |
| gpv | `git push -v` | git.plugin.sh |
| gr | `git remote` | git.plugin.sh |
| gra | `git remote add` | git.plugin.sh |
| grb | `git rebase` | git.plugin.sh |
| grba | `git rebase --abort` | git.plugin.sh |
| grbc | `git rebase --continue` | git.plugin.sh |
| grbi | `git rebase -i` | git.plugin.sh |
| grbm | `git rebase main` | git.plugin.sh |
| grbs | `git rebase --skip` | git.plugin.sh |
| grh | `git reset HEAD` | git.plugin.sh |
| grhh | `git reset HEAD --hard` | git.plugin.sh |
| grmv | `git remote rename` | git.plugin.sh |
| grrm | `git remote remove` | git.plugin.sh |
| grset | `git remote set-url` | git.plugin.sh |
| gru | `git reset --` | git.plugin.sh |
| grup | `git remote update` | git.plugin.sh |
| grv | `git remote -v` | git.plugin.sh |
| gsb | `git status -sb` | git.plugin.sh |
| gsd | `git svn dcommit` | git.plugin.sh |
| gsi | `git submodule init` | git.plugin.sh |
| gsps | `git show --pretty=short --show-signature` | git.plugin.sh |
| gsr | `git svn rebase` | git.plugin.sh |
| gss | `git status -s` | git.plugin.sh |
| gst | `git status` | git.plugin.sh |
| gsta | `git stash save` | git.plugin.sh |
| gstaa | `git stash apply` | git.plugin.sh |
| gstc | `git stash clear` | git.plugin.sh |
| gstd | `git stash drop` | git.plugin.sh |
| gstl | `git stash list` | git.plugin.sh |
| gstp | `git stash pop` | git.plugin.sh |
| gsts | `git stash show --text` | git.plugin.sh |
| gsu | `git submodule update` | git.plugin.sh |
| gts | `git tag -s` | git.plugin.sh |
| gunignore | `git update-index --no-assume-unchanged` | git.plugin.sh |
| gup | `git pull --rebase` | git.plugin.sh |
| gupv | `git pull --rebase -v` | git.plugin.sh |
| gwch | `git whatchanged -p --abbrev-commit --pretty=medium` | git.plugin.sh |
| `human_size` | `Convert into human readable value` | common.plugin.sh |
| `json2yaml` | `Convert json to yaml from stdin` | encoders.plugin.sh |
| `json_validate` | `Validate json from stdin` | encoders.plugin.sh |
| k | `kubectl` | kubernetes.plugin.sh |
| `k8s` | `Generate K8S manifests` | kubernetes.plugin.sh |
| `k8s_run` | `Get a k8s resource` | kubernetes.plugin.sh |
| `k8s_run_watch` | `watch every 1second get a k8s resource` | kubernetes.plugin.sh |
| `k8s_split` | `Split multi document kubernetes manifest file` | kubernetes.plugin.sh |
| `k8sclean` | `Delete all k8s pods from all namespaces` | kubernetes.plugin.sh |
| kal | `clear && kubectl get po,svc,no -o wide` | kubernetes.plugin.sh |
| kala | `clear && kubectl get po,svc,deploy,jobs,cronjobs,rs,cm,secret,ing,pv,pvc,no -o wide` | kubernetes.plugin.sh |
| kcj | `k8s_run cj` | kubernetes.plugin.sh |
| kcjw | `k8s_run_watch cj` | kubernetes.plugin.sh |
| kcm | `k8s_run cm` | kubernetes.plugin.sh |
| kcmw | `k8s_run_watch cm` | kubernetes.plugin.sh |
| kd | `kubectl describe` | kubernetes.plugin.sh |
| `kdec64` | `Decode base64` | encoders.plugin.sh |
| kdep | `k8s_run deploy` | kubernetes.plugin.sh |
| kdepw | `k8s_run_watch deploy` | kubernetes.plugin.sh |
| `kdo` | `macro to kill the docker desktop app and the VM (excluding vmnetd -> it's a service)` | docker.plugin.sh |
| kdp | `k8s_run deploy` | kubernetes.plugin.sh |
| kdpw | `k8s_run_watch deploy` | kubernetes.plugin.sh |
| kds | `k8s_run daemonset` | kubernetes.plugin.sh |
| kdsw | `k8s_run_watch daemonset` | kubernetes.plugin.sh |
| ked | `kubectl edit` | kubernetes.plugin.sh |
| `ken64` | `Encode base64` | encoders.plugin.sh |
| kep | `k8s_run ep` | kubernetes.plugin.sh |
| kepw | `k8s_run_watch ep` | kubernetes.plugin.sh |
| kev | `k8s_run ev` | kubernetes.plugin.sh |
| kg | `kubectl get` | kubernetes.plugin.sh |
| khpa | `k8s_run hpa` | kubernetes.plugin.sh |
| king | `k8s_run ing` | kubernetes.plugin.sh |
| kingw | `k8s_run_watch ing` | kubernetes.plugin.sh |
| `kisphp` | `Command to install `kisphp/assets`, generate `docker-compose.yml` file and manage k3d cluster` | kisphp.plugin.sh |
| kj | `k8s_run job` | kubernetes.plugin.sh |
| kjob | `k8s_run job` | kubernetes.plugin.sh |
| kjobw | `k8s_run_watch job` | kubernetes.plugin.sh |
| kjw | `k8s_run_watch job` | kubernetes.plugin.sh |
| kla | `kubectl logs --all-containers` | kubernetes.plugin.sh |
| klaf | `kubectl logs --all-containers -f` | kubernetes.plugin.sh |
| kno | `k8s_run no` | kubernetes.plugin.sh |
| know | `k8s_run_watch no` | kubernetes.plugin.sh |
| kns | `k8s_run ns` | kubernetes.plugin.sh |
| knsw | `k8s_run_watch ns` | kubernetes.plugin.sh |
| `kon` | `Get into a K8S pod with bash` | kubernetes.plugin.sh |
| `kons` | `Get into a K8S pod with shell` | kubernetes.plugin.sh |
| kpo | `k8s_run pod` | kubernetes.plugin.sh |
| kpow | `k8s_run_watch pod` | kubernetes.plugin.sh |
| kpv | `k8s_run pv` | kubernetes.plugin.sh |
| kpvc | `k8s_run pvc` | kubernetes.plugin.sh |
| kpvcw | `k8s_run_watch pvc` | kubernetes.plugin.sh |
| kpvw | `k8s_run_watch pv` | kubernetes.plugin.sh |
| krc | `k8s_run rc` | kubernetes.plugin.sh |
| krm | `kubectl delete --grace-period=0 --force --wait=false` | kubernetes.plugin.sh |
| krrs | `k8s_run rs` | kubernetes.plugin.sh |
| krrsw | `k8s_run_watch rs` | kubernetes.plugin.sh |
| krs | `k8s_run replicaset` | kubernetes.plugin.sh |
| krsw | `k8s_run_watch replicaset` | kubernetes.plugin.sh |
| ksa | `k8s_run sa` | kubernetes.plugin.sh |
| ksc | `k8s_run sc` | kubernetes.plugin.sh |
| ksec | `k8s_run secret` | kubernetes.plugin.sh |
| ksecw | `k8s_run_watch secret` | kubernetes.plugin.sh |
| kss | `k8s_run sts` | kubernetes.plugin.sh |
| kssw | `k8s_run_watch sts` | kubernetes.plugin.sh |
| ksts | `k8s_run sts` | kubernetes.plugin.sh |
| kstsw | `k8s_run_watch sts` | kubernetes.plugin.sh |
| ksv | `k8s_run svc` | kubernetes.plugin.sh |
| ksvc | `k8s_run svc` | kubernetes.plugin.sh |
| ksvcw | `k8s_run_watch svc` | kubernetes.plugin.sh |
| ksvw | `k8s_run_watch svc` | kubernetes.plugin.sh |
| ktn | `kubectl top no` | kubernetes.plugin.sh |
| ktp | `kubectl top po` | kubernetes.plugin.sh |
| kvol | `kubectl get pv,pvc -o wide` | kubernetes.plugin.sh |
| kwho | `aws sts get-caller-identity` | aws.plugin.sh |
| `mcd` | `Create directory and cd into it` | common.plugin.sh |
| mk | `minikube` | kubernetes.plugin.sh |
| `mkup` | `Start minikube` | kubernetes.plugin.sh |
| `myip` | `Show my public and private IPs` | common.plugin.sh |
| `myre` | `Restart MySQL` | server.plugin.sh |
| `ngre` | `Restart Nginx` | server.plugin.sh |
| `o` | `Open directories in Mac OS` | mac.plugin.sh |
| p2 | `python2` | python.plugin.sh |
| p3 | `python3` | python.plugin.sh |
| `penv` | `Generate python virtual env and Makefile` | python.plugin.sh |
| `pfix` | `run php cs fixer` | php.plugin.sh |
| `php2md` | `Convert php files from the current directory into markdown documentation` | php.plugin.sh |
| `php_docker` | `Generate docker directories for a PHP project` | project.plugin.sh |
| `phpre` | `Restart PHP FPM` | server.plugin.sh |
| `pinst` | `Pip install requirements.txt` | python.plugin.sh |
| `pip2up` | `Upgrade PIP` | python.plugin.sh |
| `pipfr` | `Pip export installed to requirements.txt` | python.plugin.sh |
| `pipup` | `Upgrade PIP 3` | python.plugin.sh |
| `py_docker` | `Generate docker directories for a Python project` | project.plugin.sh |
| `pycli` | `Generate a python cli application with Typer and Poetry` | python.plugin.sh |
| `r53ls` | `List route53 zones` | aws.plugin.sh |
| rmme | `rm -rf $(pwd) && cd ../` | common.plugin.sh |
| rrv | `rustrover` | rust.plugin.sh |
| `s3size` | `Calculate AWS S3 bucket size` | aws.plugin.sh |
| `saws` | `Login into EC2 ubuntu based <ip|hostname>` | aws.plugin.sh |
| `sawsa` | `Login into EC2 amazon linux based <ip|hostname>` | aws.plugin.sh |
| `sec` | `terraform output -json | jq -r ."${1}"` | terraform.plugin.sh |
| `sf` | `Run symfony console` | symfony.plugin.sh |
| `shellcheck` | `Run docker container with shellcheck executable` | devops.plugin.sh |
| `showlogs` | `Show logs in a directory (`/var/log` by default)` | logs.plugin.sh |
| `sshls` | `List SSH keys signature` | ssh.plugin.sh |
| `stree` | `Open sourcetree` | mac.plugin.sh |
| tf | `terraform` | terraform.plugin.sh |
| tfa | `terraform apply` | terraform.plugin.sh |
| tfaa | `terraform apply -auto-approve` | terraform.plugin.sh |
| `tfcost` | `Show infrastructure codes costs` | terraform.plugin.sh |
| `tfcostg` | `Generate infracost usage file` | terraform.plugin.sh |
| `tfg` | `Terraform generate files` | terraform.plugin.sh |
| tfi | `terraform init` | terraform.plugin.sh |
| tfiu | `terraform init --upgrade` | terraform.plugin.sh |
| `tflink` | `Set terraform version system wide` | terraform.plugin.sh |
| `tfls` | `List versions of terraform installed in /opt/terraform` | terraform.plugin.sh |
| `tfmod` | `Generate empty terraform module` | terraform.plugin.sh |
| `tfnew` | `Install new terraform version` | terraform.plugin.sh |
| tfo | `terraform output` | terraform.plugin.sh |
| tfoj | `terraform output -json` | terraform.plugin.sh |
| tfp | `terraform plan` | terraform.plugin.sh |
| tfr | `terraform refresh` | terraform.plugin.sh |
| tfrm | `terraform destroy` | terraform.plugin.sh |
| tfrma | `terraform destroy -auto-approve` | terraform.plugin.sh |
| tfs | `terraform show` | terraform.plugin.sh |
| tfv | `terraform validate` | terraform.plugin.sh |
| tfw | `terraform workspace` | terraform.plugin.sh |
| tg | `terragrunt` | terraform.plugin.sh |
| tga | `tg apply` | terraform.plugin.sh |
| tgaa | `tg apply -auto-approve` | terraform.plugin.sh |
| `tgd` | `Terragrunt graph-dependencies to dot file` | terraform.plugin.sh |
| `tgdi` | `Generate Terragrung dependencies png file from dot file` | terraform.plugin.sh |
| tgi | `tg init` | terraform.plugin.sh |
| tgia | `tg init && tg apply` | terraform.plugin.sh |
| tgip | `tg init && tg plan` | terraform.plugin.sh |
| tgo | `tg output` | terraform.plugin.sh |
| tgp | `tg plan` | terraform.plugin.sh |
| tgrm | `tg destroy` | terraform.plugin.sh |
| trm | `terramate` | terramate.plugin.sh |
| trmc | `terramate create` | terramate.plugin.sh |
| trmf | `terramate fmt` | terramate.plugin.sh |
| trmg | `terramate generate` | terramate.plugin.sh |
| `trmi` | `Generate terramate directory project` | terramate.plugin.sh |
| trml | `terramate list` | terramate.plugin.sh |
| trmr | `terramate run` | terramate.plugin.sh |
| v | `vim` | vim.plugin.sh |
| `venv` | `Activate python virtual environment` | python.plugin.sh |
| `vim_config` | `Run vim configuration file` | vim.plugin.sh |
| whok | `kwho` | aws.plugin.sh |
| `yaml2json` | `Convert YAML from stdin to json` | encoders.plugin.sh |
| `yaml2json_pretty` | `Convert YAML from stdin to json with pretty print` | encoders.plugin.sh |
| `yaml_validate` | `Validate YAML from stdin` | encoders.plugin.sh |