#!/usr/bin/env bash

# this function is used to display the content from manual md file with same name as called function
show_manual () {
    file="${DOTFILES}/docs/${1}.md"

    if [[ "${2}" == "--help" ]] && [[ -f $file ]];then
        dotfiles_log "show manual" "${1}"
        cat $file

        return 0
    fi

    return 1
}

read_file_content () {
    while read -r line
    do
        echo -e "${line}"
    done < "${1}"
}

_new_dir () {
    if [[ ! -d "${1}" ]]; then
        echo "Create ${1} directory"
        mkdir -p "${1}"
    else
        infoText "${1} directory already exists. ignoring"
    fi

    return 0
}

safe_copy_file () {
    if [[ ! -f "${2}" ]]; then
        echo "Copy ${1} file"
        cp "${1}" "${2}"
    else
        infoText "File ${1} already exists. Ignoring"
    fi
}

_git_ignore_element () {
    RESULT=$(grep "${1}" < .gitignore | wc -l)

    if [[ "${RESULT}" -eq 0 ]]; then
        labelText "Add ${1} to ignore list"
        gign "${1}"
    fi

    return 0
}
