#!/usr/bin/env bash

ken64 () { # Encode base64
  echo -n "${1}" | base64 -w 0
}

kdec64 () { # Decode base64
  echo -n "${1}" | base64 -w 0 -d
}

yaml_validate () { # Validate YAML from stdin
  python3 -c 'import sys, yaml, json; yaml.safe_load(sys.stdin.read())'
}

yaml2json () { # Convert YAML from stdin to json
  python3 -c 'import sys, yaml, json; print(json.dumps(yaml.safe_load(sys.stdin.read())))'
}

yaml2json_pretty () { # Convert YAML from stdin to json with pretty print
  python3 -c 'import sys, yaml, json; print(json.dumps(yaml.safe_load(sys.stdin.read()), indent=2, sort_keys=False))'
}

json_validate () { # Validate json from stdin
  python3 -c 'import sys, yaml, json; json.loads(sys.stdin.read())'
}

json2yaml () { # Convert json to yaml from stdin
  # cat file.json | json2yaml
  python3 -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))'
}
