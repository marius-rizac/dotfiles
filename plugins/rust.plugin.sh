#!/usr/bin/env bash

alias rrv='rustrover'
alias cg='cargo'
alias cgr='cargo run'
alias cgi='cargo install'
alias cgu='cargo update'
alias cgb='cargo build'
alias cgbr='cargo build --release'
alias cga='cargo add'
alias cgn='cargo new'
