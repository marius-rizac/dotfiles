#!/usr/bin/env bash

php_docker () { # Generate docker directories for a PHP project
    labelText "Create Docker directories"

    # what is the project name
    echo "What is the project name: "
    read PROJECT_NAME

    # what is the public directory name in the project
    echo "Path to public directory relative to project:"
    read PUBLIC_DIR

    echo "Copy docker containers config"
    cp -a ~/.dotfiles/templates/php_docker/config/ docker/

    echo "Copy env files"
    cp ~/.dotfiles/templates/php_docker/*.tpl.env .

    echo "Copy docker-compose file"
    cp ~/.dotfiles/templates/php_docker/docker-compose.tpl.yml .
    if [[ -f ./docker-compose.yml ]];then
      echo "Create backup for old docker-compose.yml"
      mv docker-compose.yml docker-compose.bkp.yml
    fi

    if [[ ! -f composer.json ]];then
      echo "Create composer.json file"
      cat << 'EOF' > composer.json
{
    "require": {
        "kisphp/database": "^2.0",
        "symfony/var-dumper": "^5.4"
    }
}
EOF
    fi

    if [[ ! -d ${PUBLIC_DIR} ]];then
      echo "Create ${PUBLIC_DIR} directory"
      mkdir -p ${PUBLIC_DIR}
    fi

    if [[ ! -f "${PUBLIC_DIR}/index.php" ]];then
      echo "Create ${PUBLIC_DIR}/index.php file"
      cat << 'EOF' > "${PUBLIC_DIR}/index.php"
<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Kisphp\Kisdb;

$db = Kisdb::getInstance();
$db->connect(
    getenv('DATABASE_HOST'),      // localhost
    getenv('DATABASE_USER'),      // root
    getenv('DATABASE_PASSWORD'),  // {blank}
    getenv('DATABASE_NAME')       // test
);

$q = $db->query("SHOW DATABASES");
while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
    dump($row);
}

phpinfo();
EOF
    fi

    echo "Replace variables in nginx docker file"
    mv docker/nginx/Dockerfile docker/nginx/Dockerfile.tpl
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        -e "s/project_public_dir/${PUBLIC_DIR}/g" \
        docker/nginx/Dockerfile.tpl > docker/nginx/Dockerfile
    rm docker/nginx/Dockerfile.tpl

    echo "Create docker/nginx/site.conf file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        -e "s/project_public_dir/${PUBLIC_DIR}/g" \
        docker/nginx/site.tpl.conf > docker/nginx/site.conf

    echo "Create docker-compose.yml file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        docker-compose.tpl.yml > docker-compose.yml

    echo "Create docker/database.env file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        database.tpl.env > docker/database.env

    echo "Create docker/db-admin.env file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        db-admin.tpl.env > docker/db-admin.env

    echo "Create docker/site.env file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        site.tpl.env > docker/site.env

    echo "Cleanup"
    rm docker/nginx/site.tpl.conf
    rm docker-compose.tpl.yml
    rm *.tpl.env
}

py_docker () { # Generate docker directories for a Python project
    labelText "Create Docker directories"

    # what is the project name
    echo "What is the project name: "
    read PROJECT_NAME

    echo "Copy docker containers config"
    cp -a ~/.dotfiles/templates/py_docker/config/ docker/

    echo "Copy app.dist.py file"
    cp ~/.dotfiles/templates/py_docker/app.dist.py .

    echo "Copy docker dashboard file"
    cp ~/.dotfiles/templates/py_docker/dash.tpl.sh .

    echo "Copy docker-compose file"
    cp ~/.dotfiles/templates/py_docker/docker-compose.tpl.yml .

    echo "Replace variables in template file"
    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        dash.tpl.sh > dash.sh

    sed \
        -e "s/project_name/${PROJECT_NAME}/g" \
        docker-compose.tpl.yml > docker-compose.yml

    echo "Cleanup"
    rm dash.tpl.sh
    rm docker-compose.tpl.yml
    echo "Make dashboard file executable"
    chmod u+x dash.sh
}
