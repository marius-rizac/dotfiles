#!/usr/bin/env bash

alias v='vim'

vim_config () { # Run vim configuration file
    $(which bash) ~/.dotfiles/scripts/vim-config.sh
}
