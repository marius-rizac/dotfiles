#!/usr/bin/env bash

dcpl () { # List all docker processes
    docker ps -as $@
}

function kdo() { # macro to kill the docker desktop app and the VM (excluding vmnetd -> it's a service)
  ps ax | grep -i docker | egrep -iv 'grep|com.docker.vmnetd' | awk '{print $1}' | xargs kill
}

dcshow () { # Run Apache server into a docker container with current directory as volume
  if [[ "${1}" == '' ]];then
    DIR_PATH=$(pwd)
  else
    DIR_PATH="${1}"
  fi

  if [[ "${2}" == '' ]];then
    HOST_PORT=8080
  else
    HOST_PORT="${1}"
  fi

  docker run --rm \
  -v ${DIR_PATH}:/app \
  -p ${HOST_PORT}:80 \
  -it registry.gitlab.com/kisphp/ci-registry/apache-index:latest
}

docup () { # Run docker with the provided image or ubuntu:24.04 as default
  if [[ "${1}" == '' ]];then
    IMAGE='ubuntu:24.04'
  else
    IMAGE="${1}"
  fi

  docker run \
  --rm \
  -v $(pwd):/app \
  -w ${HOME}/.composer:/root/.composer:ro \
  -w ${HOME}/.docker:/root/.docker:ro \
  -w ${HOME}/.ssh:/root/.ssh:ro \
  -w /app \
  -P \
  -it $IMAGE bash
}

dcon () { # Run docker exec with bash
    docker exec -it ${1} /bin/bash
}

dcons () { # Run docker exec with shell
    docker exec -it ${1} /bin/sh
}

dcv () { # List and remove (`-r`) docker volumes
    if [[ "${1}" == '-r' ]]; then
        docker volume rm $(docker volume ls -q)
    else
        docker volume ls
    fi
}

dcs () { # Stop all docker images
    if [[ "${1}" != '' ]]; then
        DOCKER_IMAGES="${1}"
    else
        DOCKER_IMAGES=($(docker ps -aq | tr "\n" " "))
    fi

    if [[ "${DOCKER_IMAGES}" != '' ]]; then
        dotfiles_log "stop running containers" docker
        echo "stop running containers"

        for D in $DOCKER_IMAGES
        do
            docker stop $D
        done
    else
        dotfiles_log "no container running" docker
        echo "no container running"
    fi
}

dcap () { # Start docker httpd in current directory (deprecated)
  docker run --rm -v $(pwd):/usr/local/apache2/htdocs/ -p 80:80 -t httpd:2.4
}

docrun () { # Start docker container PHP 7.4
    docker run \
        --rm \
        -v `pwd`:/project \
        -w /project \
        -t registry.gitlab.com/kisphp/ci-registry/php7.4:latest \
        /bin/bash -c "$@"
}

dcr () { # Delete stopped docker containers
    DOCKER_IMAGES=$(docker ps -aq)

    if [[ "${DOCKER_IMAGES}" != '' ]]; then
        dotfiles_log "Delete stopped containers" docker
        echo "Delete stopped containers"
        docker rm $(docker ps -aq)
    else
        dotfiles_log "No container to be deleted" docker
        echo "No container to be deleted"
    fi
}

dcimg () { # List and delete docker images
    show_manual docker $1 && return 0

    dotfiles_log "call docker images" docker
    WORD_TO_SEARCH=''
    DELETE=0
    DELETE_DANGLING=0

    for arg in "$@"
    do
        if [[ "${arg}" =~ ^- ]]; then
            if [[ "${arg}" == '-r' ]]; then
                DELETE=1
            fi
            if [[ "${arg}" == '-e' ]]; then
                DELETE_DANGLING=1
            fi
            if [[ "${arg}" == '-er' ]] || [[ "${arg}" == '-re' ]]; then
                DELETE=1
                DELETE_DANGLING=1
            fi
            if [[ "${arg}" == '-a' ]]; then
                docker rmi -f $(docker images -q)

                return 0
            fi
        else
            WORD_TO_SEARCH="${arg}"
        fi
    done

    DOCKER_IMAGES=$(docker images)

    if [[ "${WORD_TO_SEARCH}" != '' ]]; then
        DOCKER_IMAGES=$(docker images --filter=reference="*${WORD_TO_SEARCH}*")
    fi

    if [[ ${DELETE} == 1 ]] && [[ "$WORD_TO_SEARCH" != '' ]] && [[ "$DOCKER_IMAGES" != '' ]]; then
        dotfiles_log "delete images ${WORD_TO_SEARCH}" docker

        docker rmi -f $(docker images --filter=reference="*${WORD_TO_SEARCH}*" -q)
    fi

    if [[ ${DELETE_DANGLING} == 1 ]]; then
        dotfiles_log "delete dangling images" docker

        DANGLING_IMAGES=$(docker images --filter=dangling=true -q)

        if [[ "${DANGLING_IMAGES}" != '' ]]; then
            docker rmi -f $(docker images --filter=dangling=true -q)
        fi
    fi

    echo "List docker images"
    if [[ "${WORD_TO_SEARCH}" != '' ]]; then
        docker images --filter=reference="*${WORD_TO_SEARCH}*"
    else
        docker images
    fi

    return 0
}

dciup () {
    for X in $(docker images | grep latest | awk '{print $1}')
    do
      docker pull $X
    done
}

alias dcimr='docker images rm -f'
alias dcim='dcimg'
alias dci='dcimg'
alias dcl='docker ps -a'
alias dclog='docker logs'
alias dclw='watch -n 1 docker ps -a'
alias dcn='docker network'
alias dcnl='docker network ls'

alias dcup='docker compose up'
alias dcrm='docker compose rm'
alias dcst='docker compose stop'
alias dclean='docker system prune -af --volumes'
