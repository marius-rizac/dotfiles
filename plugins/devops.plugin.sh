#!/usr/bin/env bash

shellcheck () { # Run docker container with shellcheck executable
  TARGET_PATH="${1}"
  if [[ "${TARGET_PATH}" == "" ]];then
    echo -e "\nPlease provide the file to check\n"
    echo -e "Usage:\n\tshellcheck my-script.sh\n"
    return 1
  fi

  docker run --rm -v "$PWD:/mnt" koalaman/shellcheck:stable "$TARGET_PATH"
}
