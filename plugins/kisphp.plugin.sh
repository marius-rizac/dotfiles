#!/usr/bin/env bash

kisphp_k3d () {
  echo "Generate k3d setup"
  ACTION="${1}"

  if [[ "${ACTION}" == "on" ]]; then
    k3d cluster create -c ~/.dotfiles/templates/k3d/k3d.yaml
  elif [[ "${ACTION}" == "off" ]]; then
    k3d cluster delete -c ~/.dotfiles/templates/k3d/k3d.yaml
  else
    echo "Usage.."
    echo "kisphp k3d on"
    echo "kisphp k3d off"
  fi
}

kisphp () { # Command to install `kisphp/assets`, generate `docker-compose.yml` file and manage k3d cluster
  echo "$@"

  case "${1}" in
    "docker-compose")
        shift
        echo "Generate docker-compose.yml"
        cp ${DOTFILES}/templates/docker-compose.yml .
        ;;
    "assets")
        shift
        curl -s https://gitlab.com/kisphp/assets/raw/main/install.sh | bash -
        ;;
    "k3d")
        shift
        kisphp_k3d "${@}"
        ;;
    *)
        echo "Kisphp shortcuts usage:"
        echo ""
        echo -e "\t${0} docker-compose --> Create docker compose file with examples"
        echo -e "\t${0} assets --> Install kisphp assets"
        echo -e "\t${0} k3d --> Install kisphp assets"
        echo ""
        ;;
    esac
}

kh () {
  case "${1}" in
    dotfiles|df|dot)
      echo "curl https://gitlab.com/kisphp/dotfiles/raw/main/install.sh | bash -"
      ;;

    *)
      echo "Arguments: $@"
      echo "Kisphp shortcuts usage:"
      echo ""
      echo -e "\t${0} dotfiles|df|dot --> Show curl command to install dotfiles"
      echo ""
      ;;
  esac
}
