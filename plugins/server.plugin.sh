#!/usr/bin/env bash

# restart apache (ubuntu/debian)
apare () { # Restart Apache2
    dotfiles_log "Restart" "Apache2"
    sudo /etc/init.d/apache2 restart
    writeErrorMessage "Apache2 could not be restarted"
}

# restart nginx
ngre () { # Restart Nginx
    dotfiles_log "Restart" "Nginx"
    sudo /etc/init.d/nginx restart
    writeErrorMessage "Nginx could not be restarted"
}

# restart mysql
myre () { # Restart MySQL
    dotfiles_log "Restart" "MySQL"
    sudo /etc/init.d/mysql restart
    writeErrorMessage "MySQL could not be restarted"
}

elre () { # Restart Elasticsearch
    dotfiles_log "Restart" "Elasticsearch"
    sudo /etc/init.d/elasticsearch restart
    writeErrorMessage "Elasticsearch could not be restarted"
}

eltest () { # Test communication with Elasticsearch
    if [[ -z "$1" ]]; then
        EL_PORT=9200
    else
        EL_PORT="${1}"
    fi
    dotfiles_log "Test communication" "Elasticsearch"
    curl "localhost:${EL_PORT}"
}

# restart phpfpm
phpre () { # Restart PHP FPM
    if [[ $(uname) != "Linux" ]]; then
        errorText "This command must be executed only on linux machines"

        return 0
    fi
    if [[ -f '/etc/init.d/php8.2-fpm' ]]; then
        dotfiles_log "Restart version 8.2" "FPM"
        sudo /etc/init.d/php8.2-fpm restart
        writeErrorMessage "PHP 8.2 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php8.1-fpm' ]]; then
        dotfiles_log "Restart version 8.1" "FPM"
        sudo /etc/init.d/php8.1-fpm restart
        writeErrorMessage "PHP 8.1 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php8.0-fpm' ]]; then
        dotfiles_log "Restart version 8.0" "FPM"
        sudo /etc/init.d/php8.0-fpm restart
        writeErrorMessage "PHP 8.0 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php7.4-fpm' ]]; then
        dotfiles_log "Restart version 7.4" "FPM"
        sudo /etc/init.d/php7.4-fpm restart
        writeErrorMessage "PHP 7.4 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php7.3-fpm' ]]; then
        dotfiles_log "Restart version 7.3" "FPM"
        sudo /etc/init.d/php7.3-fpm restart
        writeErrorMessage "PHP 7.3 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php7.2-fpm' ]]; then
        dotfiles_log "Restart version 7.2" "FPM"
        sudo /etc/init.d/php7.2-fpm restart
        writeErrorMessage "PHP 7.2 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php7.1-fpm' ]]; then
        dotfiles_log "Restart version 7.1" "FPM"
        sudo /etc/init.d/php7.1-fpm restart
        writeErrorMessage "PHP 7.1 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php7-fpm' ]]; then
        dotfiles_log "Restart version 7" "FPM"
        sudo /etc/init.d/php7-fpm restart
        writeErrorMessage "PHP 7 FPM could not be restarted"

        return 0
    fi
    if [[ -f '/etc/init.d/php8-fpm' ]]; then
        dotfiles_log "Restart version 8" "FPM"
        sudo /etc/init.d/php8-fpm restart
        writeErrorMessage "PHP 8 FPM could not be restarted"

        return 0
    fi

    errorText "No php executable found"
}
