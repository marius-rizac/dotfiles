edit () { # Start IntelliJ editors in current or specified directory
    if [[ "${1}" == "-h" ]]; then
        printf "\n%s:\n" "Usage"
        echo "${0} [directory_name]  -> open IntelliJ Editor in specified directory"
        echo "${0} .                 -> open IntelliJ Editor in current directory"
        echo " "
        return 1
    elif [[ "${1}" != "" ]]; then
        # IntelliJ Idea must always exists
        EDITOR_IDEA=$(which idea)
        EDITOR_PHP=$(which pstorm)
        if [[ $EDITOR_PHP == *"not found"* ]]; then
          EDITOR_PHP=${EDITOR_IDEA}
        fi
        EDITOR_PYTHON=$(which charm)
        if [[ $EDITOR_PYTHON == *"not found"* ]]; then
          EDITOR_PYTHON=${EDITOR_IDEA}
        fi
        EDITOR_RUST=$(which rustrover)
        if [[ $EDITOR_RUST == *"not found"* ]]; then
          EDITOR_RUST=${EDITOR_IDEA}
        fi
        EDITOR_RUBY=$(which rubymine)
        if [[ $EDITOR_RUBY == *"not found"* ]]; then
          EDITOR_RUBY=${EDITOR_IDEA}
        fi

        if [[ -f "${1}/Cargo.toml" ]]; then
          dotfiles_log "I am in directory $(pwd) and I open RustRover: ${1}"
          $EDITOR_RUST "${1}"
        elif [[ -f "${1}/composer.json" ]]; then
          dotfiles_log "I am in directory $(pwd) and I open PhpStorm: ${1}"
          $EDITOR_PHP "${1}"
        elif [[ -f "${1}/main.py" ]] || [[ -d "${1}/.venv" ]] || [[ -d "${1}/venv" ]] || [[ -f "${1}/pyproject.toml" ]]; then
          dotfiles_log "I am in directory $(pwd) and I open PyCharm: ${1}"
          $EDITOR_PYTHON "${1}"
        else
          dotfiles_log "I am in directory $(pwd) and I open IntelliJ: ${1}"
          $EDITOR_IDEA "${1}"
        fi
        return 0
    else
        dotfiles_log "I am in directory $(pwd) and I open IntelliJ"
        idea .
        return 0
    fi
}
