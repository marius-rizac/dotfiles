#!/usr/bin/env bash

alias cc='composer'
alias ccdu='composer dump-autoload'
alias cci='composer install'
alias ccr='composer require'
alias ccst='composer status'
alias ccu='composer update'
alias ccw='composer why'
alias ccwn='composer why-not'

# run php code fixer
pfix () { # run php cs fixer
    if [[ -f 'vendor/bin/php-cs-fixer' ]]; then
        php vendor/bin/php-cs-fixer fix -v
    fi

    if [[ -f 'bin/php-cs-fixer' ]]; then
        php bin/php-cs-fixer fix -v
    fi
}

php2md () { # Convert php files from the current directory into markdown documentation
  if [[ $(which php) == "" ]]; then
    echo -e "\n!! Please install PHP 7+ to use this script !!\n"
  else
    curl -s https://gitlab.com/-/snippets/2212663/raw/main/project2md.php | php
  fi
}

# run codeception
codecept () { # Run codecept
    if [[ -f 'vendor/bin/codecept' ]]; then
        php vendor/bin/codecept $@
    fi

    if [[ -f 'bin/codecept' ]]; then
        php bin/codecept $@
    fi
}

debug () { # Export php debug configurations
    show_manual debug $1 && return 0

    while getopts ":h:n:" opt; do
        case $opt in
            h)
                debug_host=${OPTARG}
                dotfiles_log "set debug_host = ${debug_host}" debug
                ;;
            n)
                debug_name=${OPTARG}
                dotfiles_log "set debug_name = ${debug_name}" debug
                ;;
        esac
    done

    if [ -z "${debug_name}" ];then
        debug_name='localhost'
        dotfiles_log "set debug_name = ${debug_name}" debug
    fi
    if [ -z "${debug_host}" ];then
        debug_host='0.0.0.0'
        dotfiles_log "set debug_host = ${debug_host}" debug
    fi

    export XDEBUG_CONFIG="remote_host=${debug_host}"
    export PHP_IDE_CONFIG="serverName=${debug_name}"
}
