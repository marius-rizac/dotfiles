data "aws_caller_identity" "current" {}

#terraform {
#  required_version = "~> 1.1"
#  backend "s3" {
#    bucket = "${data.aws_caller_identity.current.id}-terraform-state"
#    key = "my-project/state.tf"
#    region = "us-east-1"
#    profile = "la"
#  }
#}

# Use if necessary
#data "terraform_remote_state" "web-1" {
#  backend = "s3"
#
#  config = {
#    bucket = "${data.aws_caller_identity.current.id}-terraform-state"
#    key = "my-project/state.tf"
#    region = "us-east-1"
#    profile = "la"
#  }
#}

provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Generator = "Terraform"
      Environment = "test"
    }
  }
}
