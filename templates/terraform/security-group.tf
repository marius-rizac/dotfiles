variable "ec2_name" {
  type = string
  default = "test-ec2"
  description = "(Optional) test ec2"
}

resource "aws_security_group" "ec2" {
  name = var.ec2_name
  vpc_id = try(data.aws_vpc.vpc.id, null)

  tags = {
    Name = var.ec2_name
  }

  ingress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0

    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0

    cidr_blocks = ["0.0.0.0/0"]
  }
}
