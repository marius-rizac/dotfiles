variable "key-pair-name" {
  type = string
  default = "ec2-ssh-pub-key"
  description = "(Required) The ssh public key name to use with EC2 instances"
}

resource "aws_key_pair" "ssh" {
  public_key = file("~/.ssh/id_rsa.pub")
  key_name = var.key-pair-name
}
