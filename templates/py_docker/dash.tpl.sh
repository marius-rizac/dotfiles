#!/usr/bin/env bash

NETWORK='project_name'

docker_build () {
    if [[ $(docker network ls -q -f name=${NETWORK}) == '' ]]; then
        echo "Creating network"
        docker network create $NETWORK
    fi

    echo "Create PHP container"
    docker build --compress -t "${NETWORK}-flask" -f docker/python/Dockerfile .
}

docker_run () {
    docker run \
        --rm \
        -d \
        --name="${NETWORK}-flask" \
        --network="${NETWORK}" \
        -v `pwd`:/project \
        -w /project \
        -p "80:5000" \
        -t "${NETWORK}-flask"
}

docker_db () {
    docker run \
        --rm \
        -d \
        --name="${NETWORK}-sql" \
        --network="${NETWORK}" \
        -v `pwd`/_data/mysql:/var/lib/mysql \
        -e MYSQL_ROOT_PASSWORD=project_name \
        -e MYSQL_USER=project_name \
        -e MYSQL_PASSWORD=project_name \
        -e MYSQL_DATABASE=project_name \
        -t mysql:5.7
}

docker_adm () {
    docker run \
        --rm \
        -d \
        -e PMA_HOST="${NETWORK}-sql" \
        --network="${NETWORK}" \
        --name="${NETWORK}-phpmyadmin" \
        -p 8080:80 \
        -t phpmyadmin/phpmyadmin

    echo " "
    echo " OPEN URL: http://localhost:8080 "
    echo " "
}

if [[ "${1}" == "build" ]]; then
    docker_build
fi

if [[ "${1}" == "run" ]]; then
    docker_run
fi

if [[ "${1}" == "db" ]]; then
    docker_db
fi

if [[ "${1}" == "adm" ]]; then
    docker_adm
fi

if [[ "${1}" == "" ]]; then
    echo "Please choose:"
    echo "  ${0} build - Build docker images"
    echo "  ${0} run   - Run application"
    echo "  ${0} db    - Run mysql 5.7 container in the network"
    echo "  ${0} adm   - Run phpmyadmin container to access database"
    echo " "
fi
