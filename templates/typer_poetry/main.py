import typer
from rich import print as rprint
from rich.console import Console
from rich.table import Table
from typing_extensions import Annotated

console = Console()

app = typer.Typer(no_args_is_help=True, rich_markup_mode="rich")


@app.command(name="fetch", help="Fetch all PRs from repositories")
def fetch(
        search: Annotated[str, typer.Argument()] = None,
):
    """
    Fetch all PRs from repositories
    """

    rprint(f"Search: [bold]{search}")


@app.command(name="view", help="View list of PRs", no_args_is_help=False)
def view(
        search: Annotated[str, typer.Argument()] = None,
        update: Annotated[bool, typer.Option('--update', '-u')] = False,
):
    table = Table("Kind", "Name", "Validation")

    rprint(f"Search: [bold]{search}")
    rprint(f"Update: [bold]{update}")

    console.print(table)


if __name__ == '__main__':  # pragma: no cover
    typer.run(app)
